package com.saiflimited.nodemcu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.saiflimited.nodemcu.bean.Credential;
import com.saiflimited.nodemcu.interfaces.NodeMCUApi;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class SSIDFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String TAG = "SSIDFRAGment";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    NodeMCUApi mApi;
    RecyclerView recyclerView;
    ProgressDialog progressDialog;
    WifiScanReceiver wifiReciever;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SSIDFragment() {
    }

    WifiManager wifiManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        mApi = ((NodeMCUApplication) getContext().getApplicationContext()).getApiService();
        wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiReciever = new WifiScanReceiver();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ssid_list, container, false);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Getting all the hotspots");
        progressDialog.setMessage("Please wait ...");

        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new MySSIDRecyclerViewAdapter(mListener, this));
        getAllSSIDs();

        Button refresh = view.findViewById(R.id.refresh);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllSSIDs();
            }
        });
        return view;
    }

    public void getAllSSIDs() {
        progressDialog.show();
        wifiManager.startScan();
//        mApi.getAllSSIDs()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(strings -> showAllSSIDList(strings.ssids), new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        throwable.printStackTrace();
//                    }
//                });
    }

    public void askForNetworkPassword(String ssid) {
        new MaterialDialog.Builder(getContext())
                .content(R.string.enter_password)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(R.string.enter_password, R.string.blank, false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {

                    }
                })
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .onPositive((dialog, which) -> {
                    connectNodeMCUToWifi(ssid, dialog.getInputEditText().getText().toString());
                })
                .onNegative((dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }

    @SuppressLint("CheckResult")
    private void connectNodeMCUToWifi(String ssid, String password) {
        Credential credential = new Credential();
        credential.username = ssid;
        credential.password = password;
        mApi.connectNodeMCUToWifi(credential)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(strings -> getMQTTConfig(), throwable -> {
                    throwable.printStackTrace();
                    Toast.makeText(getContext(), "Unable to connect", Toast.LENGTH_LONG).show();
                });
    }

    private void finishAndSendData() {
        getMQTTConfig();
        Intent resultIntent = new Intent();
        getActivity().setResult(Activity.RESULT_OK, resultIntent);
        getActivity().finish();
    }

    private void getMQTTConfig() {
        Log.d(TAG, "connectNodeMCUToWifi: ");
        mApi.getMQTTConfig()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mqtt -> {
                    SharedPrefUtils.storedMQTT(getActivity(), mqtt);
                    finishAndSendData();
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                    }
                });

    }


    private void showAllSSIDList(List<String> strings) {
        progressDialog.dismiss();
        ((MySSIDRecyclerViewAdapter) recyclerView.getAdapter()).addData(strings);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(String item);
    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().unregisterReceiver(wifiReciever);
    }

    @Override
    public void onResume() {
        super.onResume();
        getContext().registerReceiver(wifiReciever, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }

    class WifiScanReceiver extends BroadcastReceiver {
        @SuppressLint("UseValueOf")
        public void onReceive(Context c, Intent intent) {
            boolean success = intent.getBooleanExtra(
                    WifiManager.EXTRA_RESULTS_UPDATED, false);
            if(success) {
                List<ScanResult> mScanResults = wifiManager.getScanResults();
                List<String> ssids = new ArrayList<>();
                for (ScanResult scanResult : mScanResults) {
                    ssids.add(scanResult.SSID);
                }
                showAllSSIDList(ssids);
            } else {
                Log.d(TAG, "onReceive: Scan failed");
            }

        }
    }


}
