package com.saiflimited.nodemcu;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.saiflimited.nodemcu.bean.MQTT;

public class SharedPrefUtils {
    private static final String sharedPrefName = "nodemcu";
    private static final String MQTT_PREF = "mqtt";

    public static MQTT getStoredMQTT(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE);
        MQTT mqtt = new Gson().fromJson(sharedPreferences.getString(MQTT_PREF, null), MQTT.class);
        return mqtt;
    }

    public static void storedMQTT(Context context,MQTT mqtt) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(MQTT_PREF,new Gson().toJson(mqtt)).commit();
    }
}
