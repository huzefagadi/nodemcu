package com.saiflimited.nodemcu;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.saiflimited.nodemcu.SSIDFragment.OnListFragmentInteractionListener;

import java.util.ArrayList;
import java.util.List;

public class MySSIDRecyclerViewAdapter extends RecyclerView.Adapter<MySSIDRecyclerViewAdapter.ViewHolder> {

    private List<String> mValues;
    private final OnListFragmentInteractionListener mListener;
    SSIDFragment fragment;
    public MySSIDRecyclerViewAdapter(OnListFragmentInteractionListener listener,SSIDFragment fragment) {
        mValues = new ArrayList<>();
        mListener = listener;
        this.fragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_ssid, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.askForNetworkPassword(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void addData(List<String> strings) {
        this.mValues = strings;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public String mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.ssid);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mIdView.getText() + "'";
        }
    }
}
