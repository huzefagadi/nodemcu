package com.saiflimited.nodemcu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class NetworkSearchActivity extends AppCompatActivity implements SSIDFragment.OnListFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_search);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new SSIDFragment()).commit();
    }

    @Override
    public void onListFragmentInteraction(String item) {

    }
}
