package com.saiflimited.nodemcu.interfaces;


import com.saiflimited.nodemcu.bean.SSIDResponse;
import com.saiflimited.nodemcu.bean.Credential;
import com.saiflimited.nodemcu.bean.MQTT;

import org.json.JSONObject;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface NodeMCUApi {

    String URL = "http://192.168.4.1/";

    /*********************** USERS ***********************/
    @GET("list")
    Observable<SSIDResponse> getAllSSIDs();

    @POST("connectwifi")
    Observable<String> connectNodeMCUToWifi(@Body Credential credential);

    @GET("delete")
    Observable<JSONObject> deleteSettings();


    @GET("mqtt")
    Observable<MQTT> getMQTTConfig();

//    @GET("categories/{categoryId}/wallpapers")
//    Observable <ApiResponse<List<WallpaperImage>>>getAllImagesForCategoryId(@Path("categoryId") String categoryId);
//
//    @PUT("categories/{categoryId}/wallpapers/{wallpaperId}/set")
//    Observable <ApiResponse<String>>notifyWallpaperSet(@Path("categoryId") String categoryId,@Path("wallpaperId") String wallpaperId);

}