package com.saiflimited.nodemcu;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.saiflimited.nodemcu.bean.Credential;
import com.saiflimited.nodemcu.bean.MQTT;
import com.saiflimited.nodemcu.interfaces.NodeMCUApi;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class SettingsActivity extends AppCompatActivity {
    private static final int REQUEST_CHECK_SETTINGS = 102;
    private int retries = 0;
    private static final String TAG = SettingsActivity.class.getName();
    private static final int REQ_NETWORK_NAME = 20;
    private static final int PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 2;
    String plainSSID = "ESPap";
    String SSID = String.format("\"%s\"", plainSSID);
    String PASSWORD = String.format("\"%s\"", "thereisnospoon");
    WifiManager wifiManager;
    WifiReceiver receiverWifi;
    NodeMCUApi mApi;
    public MQTT mqtt;
    StringBuilder sb = new StringBuilder();
    boolean connect = false;
    String currentConnectedWifiSSID, currentConnectedWifiPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        receiverWifi = new WifiReceiver();
        mApi = ((NodeMCUApplication) getApplicationContext()).getApiService();
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);


        Button connectToMCU = findViewById(R.id.connect_to_mcu);
        connectToMCU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                            PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION);
                    //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method

                } else {
                    //  showList = true;
                    getCurrentWifiInfoFromUser();
                }

            }
        });


        Button deleteSettingsButton = findViewById(R.id.deleteSettings);
        deleteSettingsButton.setOnClickListener(v -> new MaterialDialog.Builder(SettingsActivity.this)
                .content(R.string.confirm_delete)
                .positiveText(R.string.yes)
                .negativeText(R.string.no)
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    deleteSettings();
                })
                .onNegative((dialog, which) -> {
                    dialog.dismiss();
                })
                .show());
    }

    private void getCurrentWifiInfoFromUser() {
        if (checkIfConnectedToWifi()) {
            askForNetworkPassword();
        } else {
            Toast.makeText(SettingsActivity.this, getString(R.string.please_connect_to_wifi), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        intentFilter.setPriority(10000);
        registerReceiver(receiverWifi, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiverWifi);
    }

    private boolean checkIfConnectedToWifi() {
        WifiInfo info = wifiManager.getConnectionInfo();
        boolean status = info == null || info.getSupplicantState() == null || info.getSupplicantState().equals(SupplicantState.DISCONNECTED);
        return !status;
    }

    public void askForNetworkPassword() {
        new MaterialDialog.Builder(this)
                .content(R.string.enter_password)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(R.string.enter_password, R.string.blank, false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {

                    }
                })
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .onPositive((dialog, which) -> {
                    currentConnectedWifiPassword = dialog.getInputEditText().getText().toString();
                    displayLocationSettingsRequest(this);
                })
                .onNegative((dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }


//    public void connectToNodeMcu(String ssid, String passkey) {
//        Log.i(TAG, "* connectToAP");
//        connect = true;
//        WifiConfiguration wifiConfiguration = new WifiConfiguration();
//
//        String networkSSID = ssid;
//        String networkPass = passkey;
//
//        Log.d(TAG, "# password " + networkPass);
//
//
//        wifiConfiguration.SSID = "\"" + networkSSID + "\"";
//        wifiConfiguration.wepKeys[0] = "\"" + networkPass + "\"";
//        wifiConfiguration.wepTxKeyIndex = 0;
//        wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
//        wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
//        int res = wifiManager.addNetwork(wifiConfiguration);
//        Log.d(TAG, "### 1 ### add Network returned " + res);
//
//        boolean b = wifiManager.enableNetwork(res, true);
//        Log.d(TAG, "# enableNetwork returned " + b);
//
//        wifiManager.setWifiEnabled(true);
//
//
//        wifiConfiguration.SSID = "\"" + networkSSID + "\"";
//        wifiConfiguration.preSharedKey = "\"" + networkPass + "\"";
//        wifiConfiguration.hiddenSSID = true;
//        wifiConfiguration.status = WifiConfiguration.Status.ENABLED;
//        wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
//        wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
//        wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
//        wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
//        wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
//        wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
//        wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
//
//        res = wifiManager.addNetwork(wifiConfiguration);
//        Log.d(TAG, "### 2 ### add Network returned " + res);
//
//        wifiManager.enableNetwork(res, true);
//
//        boolean changeHappen = wifiManager.saveConfiguration();
//
//        if (res != -1 && changeHappen) {
//            Log.d(TAG, "### Change happen");
//
//            currentConnectedWifiSSID = networkSSID;
//
//        } else {
//            Log.d(TAG, "*** Change NOT happen");
//        }
//
//        wifiManager.setWifiEnabled(true);
//    }
//}
//    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    void connectToNodeMcu() {
        connect = true;
//        Settings.Global.putInt(getContentResolver(), "captive_portal_detection_enabled", 0);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (retries == 0 && wifiInfo.getSupplicantState() == SupplicantState.COMPLETED) {
            currentConnectedWifiSSID = wifiInfo.getSSID().replaceAll("\"", "");
        }
        retries++;
        if (wifiManager.isWifiEnabled() == false) {
            wifiManager.setWifiEnabled(true);
        }
        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = SSID;
        wifiConfig.preSharedKey = PASSWORD;
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        wifiConfig.priority = 1000000;
        int netId = wifiManager.addNetwork(wifiConfig);
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();

        NetworkRequest.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new NetworkRequest.Builder();
            builder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI);
            ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            connectivityManager.requestNetwork(builder.build(), new ConnectivityManager.NetworkCallback() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onAvailable(Network network) {
                    connectivityManager.bindProcessToNetwork(network);
                }
            });
        }

    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(result1 -> {
            final Status status = result1.getStatus();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    Log.i(TAG, "All location settings are satisfied.");
                    connectToNodeMcu();
                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                    try {
                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        status.startResolutionForResult(SettingsActivity.this, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException e) {
                        Log.i(TAG, "PendingIntent unable to execute request.");
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                    break;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == Activity.RESULT_OK) {
            connectToNodeMcu();
        } else {
            Toast.makeText(this, "Location services are required for the app to work", Toast.LENGTH_SHORT).show();
        }
    }


    class WifiReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            if (intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                Log.d(TAG, "onReceive: " + info.getDetailedState());
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                Log.d("wifissid", wifiInfo.getSSID());
                if (info.getDetailedState().equals(NetworkInfo.DetailedState.CONNECTED) && wifiInfo != null && connect) {
                    if (SSID.equals(wifiInfo.getSSID())) {
                        connect = false;
                        retries = 0;
                        new Handler().postDelayed(() -> getMQTTConfig(), 1000); // just give time for wifi to connect properly

                    } else {
                        if (retries < 15) {
                            connectToNodeMcu();
                        } else {
                            Toast.makeText(SettingsActivity.this, "Unable to connect to nodemcu", Toast.LENGTH_LONG).show();
                        }

                    }

                    // TODO: 08/03/19 FIGURE THIS OUT AND SHOW LIST
//                    Intent intent1 = new Intent(SettingsActivity.this, NetworkSearchActivity.class);
//                    startActivityForResult(intent1, REQ_NETWORK_NAME);
//                    showList = false;
                }
            }
        }

    }

    @SuppressLint("CheckResult")
    private void connectNodeMCUToWifi() {
        Credential credential = new Credential();
        credential.username = currentConnectedWifiSSID;
        credential.password = currentConnectedWifiPassword;
        mApi.connectNodeMCUToWifi(credential)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(strings -> {
                    Log.d(TAG, "connectNodeMCUToWifi: " + strings);
                    disconnectFromWifiAndGoShowSuccessMessage();
                }, throwable -> {
                    throwable.printStackTrace();
                    disconnectFromWifiAndGoShowSuccessMessage();
                    //  Toast.makeText(this, "Unable to connect", Toast.LENGTH_LONG).show();
                });
    }

    private void disconnectFromWifiAndGoShowSuccessMessage() {
        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = currentConnectedWifiSSID;
        wifiConfig.preSharedKey = currentConnectedWifiPassword;
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        wifiConfig.priority = 1000000;
        int netId = wifiManager.addNetwork(wifiConfig);
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();
        Toast.makeText(this, "Connection successful, please connect to mqtt", Toast.LENGTH_LONG).show();
    }

    private void getMQTTConfig() {
        Log.d(TAG, "connectNodeMCUToWifi: ");
        mApi.getMQTTConfig()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mqtt -> {
                    SharedPrefUtils.storedMQTT(this, mqtt);
                    connectNodeMCUToWifi();
                }, throwable -> {
                    throwable.printStackTrace();
                    Toast.makeText(SettingsActivity.this, throwable.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                });

    }


    void deleteSettings() {
        mApi.deleteSettings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(strings -> Log.d(TAG, "deleteSettings: " + strings), throwable -> throwable.printStackTrace());
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION
                && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            // Do something with granted permission
            getCurrentWifiInfoFromUser();
        } else {
            Toast.makeText(this, "All the permissions are necessary", Toast.LENGTH_LONG).show();
        }
    }


}
